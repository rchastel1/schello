;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2023 Romain Châtel
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(library (schello)
  (export schello schello-version)
  (import (rnrs (6))
          (rnrs control (6)))

  (define (schello-version) "1.0.0")

  (define schello
    (case-lambda
      (() (schello "Hello, world!"))
      ((text-or-traditional)
       (case text-or-traditional
         ((#t) (schello "hello, world"))
         ((#f) (schello))
         (else
          (unless (string? text-or-traditional)
            (assertion-violation 'schello
                                 "Argument must be either a boolean or a string"
                                 text-or-traditional))
          (display text-or-traditional) (newline))))))
  )
