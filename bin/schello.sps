
;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2023 Romain Châtel
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(import (rnrs (6))
        (rnrs programs (6))
        (rnrs mutable-pairs (6))
        (schello))

(define (main cmdline)

  (define greeting '(greeting "--greeting=" "g"))
  (define traditional '(traditional "--traditional" "t"))
  (define help '(help "--help" "h"))
  (define version '(version "--version" "v"))
  (define bad-option `(,(car help) "-" #f))

  (define (small-spec opt-spec) (list (car opt-spec) (caddr opt-spec)))

  (define (string-match txt pat)
    (let ((txt-len (string-length txt))
          (pat-len (string-length pat)))
      (let loop ((i 0))
        (cond
         ((= i pat-len) i)
         ((= i txt-len) #f)
         ((char=? (string-ref txt i) (string-ref pat i)) (loop (+ i 1)))
         (else #f)))))

  (define (match-option opt-spec match-proc)
    (lambda (arg)
      (let ((m (string-match arg (cadr opt-spec))))
        (if m (list (cons (car opt-spec) (match-proc arg m))) #f))))

  (define (match-boolean-option opt-spec)
    (match-option opt-spec (lambda _ #t)))
  
  (define match-greeting
    (match-option
     greeting
     (lambda (arg m) (substring arg m (string-length arg)))))
  (define match-traditional (match-boolean-option traditional))
  (define match-help (match-boolean-option help))
  (define match-version (match-boolean-option version))
  (define match-bad-option (match-boolean-option bad-option))
  (define match-h (match-boolean-option (small-spec help)))
  (define match-v (match-boolean-option (small-spec version)))
  (define match-t (match-boolean-option (small-spec traditional)))
  (define match-g (match-option (small-spec greeting) (lambda _ "")))

  (define (match-small-options arg)
    (and (string-match arg "-")
         (let match-letter ((arg (substring arg 1 (string-length arg)))
                            (matched (list)))
           (if (> (string-length arg) 0)
               (let ((m (or (match-h arg) (match-v arg)
                            (match-t arg) (match-g arg)
                            #f)))
                 (and m (match-letter (substring arg 1 (string-length arg))
                                      (cons (car m) matched))))
               matched))))

  (define (parse-arg arg)
    (or (match-greeting arg)
        (match-traditional arg)
        (match-help arg)
        (match-version arg)
        (match-small-options arg)
        (match-bad-option arg)
        arg))

  (define (print-usage)
    (display "Usage: ") (display (car cmdline)) (display " [OPTION]...")
    (newline)
    (display "Print a friendly, customizable greeting.")
    (newline)
    (newline)
    (display "-h, --help          display this help and exit")
    (newline)
    (display "-v, --version       display version information and exit")
    (newline)
    (display "-t, --traditional       use traditional greeting")
    (newline)
    (display "-g, --greeting=TEXT     use TEXT as the greeting message")
    (newline)
    (newline))

  (define (print-version)
    (display "schello ") (display (schello-version))
    (newline)
    (newline)
    (display "Copyright (C) 2023 Romain Châtel")
    (newline)
    (display "License GPLv3+: GNU GPL version 3 or later ")
    (display "<http://gnu.org/licenses/gpl.html>")
    (newline)
    (display
     "This is free software: you are free to change and redistribute it.")
    (newline)
    (display "There is NO WARRANTY, to the extent permitted by law.")
    (newline))

  (let parse-args ((cmdargs (cdr cmdline))
                   (args (list)))
    (if (null? cmdargs)
        (cond
         ((assq (car help) args) (print-usage) (exit 0))
         ((assq (car version) args) (print-version) (exit 0))
         ((assp (lambda (key)
                  (or (eq? key (car traditional)) (eq? key (car greeting))))
                args) =>
                (lambda (kv)
                  (case (car kv)
                    ((traditional) (schello #t) (exit 0))
                    (else (schello (cdr kv)) (exit 0)))))
         ((null? args) (schello) (exit 0))
         (else (print-usage) (exit 1)))
        (let* ((cmdarg (car cmdargs))
               (arg (parse-arg cmdarg)))
          (parse-args
           (cdr cmdargs)
           (cond
            ((list? arg) (append arg args))
            ((string? arg)
             (let ((last-greeting (assq (car greeting) args)))
               (if last-greeting
                   (let* ((str (cdr last-greeting))
                          (sep (if (> (string-length str) 0) " " "")))
                     (set-cdr! last-greeting
                               (string-append str sep arg))
                     args)
                   (cons (cons (car greeting) arg) args))))
            (else args)))))))

(main (command-line))
