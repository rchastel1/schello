;; -*- mode: scheme; coding: utf-8 -*-
;; Copyright © 2023 Romain Châtel
;; SPDX-License-Identifier: GPL-3.0-or-later

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

#!r6rs

(import (rnrs (6))
        (schello)
        (srfi :39 parameters)
        (srfi :64 testing)
        )

(unless (test-runner-current) (test-runner-current (test-runner-simple)))

(define (schello->string . args)
  (call-with-string-output-port
   (lambda (port)
     (parameterize ([current-output-port port])
       (apply schello args)))))

(test-group "schello"
  (test-equal "Hello, world!\n" (schello->string))
  (test-equal "Hello, world!\n" (schello->string #f))
  (test-equal "hello, world\n" (schello->string #t))
  (test-equal "Hello, wonderful world!\n"
    (schello->string "Hello, wonderful world!"))
  )

(exit (if (zero? (test-runner-fail-count (test-runner-get))) 0 1))
